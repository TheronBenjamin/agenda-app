# Objectifs
Lors de cet atelier, l’apprenant doit :  
-  A partir des diagrammes de classes et des diagrammes de composants d’une étude  de cas donnée par l’intervenant, développer et tester les composants de la couche  métier d’une application informatique.

## Sujet
A partir du diagramme de classe suivant , implémentez dans le langage de votre choix la structure logicielle correspondante.

## Cahier des charges:

Nous désirons implanter la gestion d’un agenda:
- un agenda contient un ensemble de personnes nommées `contacts`.
- Un agenda possède un `owner` possédant un login/mot de passe et jwt token.
- Chaque contact est identifié par son nom et par un ensemble de coordonnées
- Une coordonnée peut être postale, téléphonique ou électonique (email ou page web)
- Chaque coordonnées possède une méthode propre permettant de valider le format de saisie. 

![Diagramme de classe](Class_diag.png)


## Attendus de l'exercice
Dans le langage de votre choix, implémentez les classes liées à ce diagramme et commencez à créer un premier agenda statique. 

Vérifiez l'ensemble des méthodes nécessaires pour que l'agenda soit opérationnel.

A partir du code produit, réalisez le diagramme de classe final.

## Bonus possibles
 - Implémenter des tests unitaires ou fonctionnels au besoin.
 - Ajouter une partie afin de persister les données.
 - Ajouter une api
 - Implémenter un client communiquant avec un back-end simulé.

 

## Livrables attendus
Livraison par le biais d'un dépôt git communiqué au formateur contenant :
 - un fichier README.md expliquant à minima :
    - comment installer lancer le projet 
    - comment lancer un environnement de dev.
 - le code source de l'application
 - le diagramme de classe final

## Commande pour run le projet (java 17):
Un fois au sein du dossier du projet réaliser les commandes suivantes:
- Pour information nous avons fourni un jeu de données avec un utilisateur root. Une fois dans la console, il faut se connecter avec(login: root, mdp: root). il est ensuite possible de voir ses agendas avec les contacts
- cd target
- java -cp agenda-app-1.0-SNAPSHOT.jar ../src\main\java\Main.java 


** Parcour à réaliser : ** 
  - choisir : "login with your username and password"
  - Entrez "root" pour le login
  - Entrez "root" pour le mot de passe
  - Entrez "2" pour "Use an existing agenda"
  - (La liste des agendas et des contacts apparaitrons)
  - Au dela de ce parcours il est possible de naviguer dans le programme sans difficultées.


![Diagramme de classe](diag class modifie.png)
