import Entity.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        List<User> userList= InitializeUser.Initialize();
        for (User user : userList) {
            System.out.println(user);
        }

        Scanner scanner = new Scanner(System.in);
        String userChoice = String.valueOf(99999);
        User connectedUser = null;
        List<Agenda> agendaList = new ArrayList<>();


        while(!userChoice.equals("0")) {
            userChoicesToCreateOrConnectUser();
            userChoice = scanner.nextLine();
            if (userChoice.equals("1")) {
                //TODO: implement function
                connectedUser = createUser(userList);
            } else if (userChoice.equals("2")){
                connectedUser = connectWithExistingUser(userList);
                while (!userChoice.equals("0")){
                    userChoicesForAgenda();
                    userChoice = scanner.nextLine();
                    if(userChoice.equals("1")) {
                        System.out.println("Enter an agenda name");
                        String userAgenda = scanner.nextLine();
                        Agenda agenda = new Agenda(userAgenda, null);
                        agendaList.add(agenda);
                        for(Agenda agenda1 : agendaList) {
                            System.out.println(agenda1);
                        }
                        connectedUser.setAgendas(agendaList);
                        System.out.println();
                    } else if (userChoice.equals("2")) {
                        System.out.println("View agenda");
                        for(Agenda agenda1 : connectedUser.getAgendas()) {
                            System.out.println(agenda1);
                        }
                        //TODO: implement function
                        userChoicesForContact();
                        userChoice = scanner.nextLine();
                        System.out.println();
                    } else if (userChoice.equals("0")) {
                        break;
                    }
                    else {
                        System.out.println("Please enter a valid number");
                    }
                }
            } else if (userChoice.equals("0")) {
                System.out.println("Thx for using the applciation bye !");
                break;
            } else {
                System.out.println("Please enter a valid number");
                System.out.println();
            }
        }

    }

    private static void userChoicesForContact() {
        System.out.println("Choices");
        System.out.println("1. create a contact");
        System.out.println("2. see contacts");
        System.out.println();
    }

    private static void userChoicesForAgenda() {
        System.out.println("Choices");
        System.out.println("1. create an Agenda");
        System.out.println("2. Use an existing  Agenda");
        System.out.println();
    }

    private static void userChoicesToCreateOrConnectUser() {
        System.out.println();
        System.out.println("Enter a number between 1 and 0");
        System.out.println("Login with root user with your username = root and password = root");
        System.out.println("1. Create a new user");
        System.out.println("2. Login with your username and password");
        System.out.println("0. Exit");
        System.out.println();
    }

    private static User createUser(List<User> userList) {
        User user = new User();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Entrez votre login:");
        String login = scanner.nextLine();
        System.out.println("Entrez votre password:");
        String password = scanner.nextLine();
        user.setLogin(login);
        user.setPassword(password);
        user.setJwt_token("token");
        userList.add(user);
        System.out.println("User created with login : " + user.getLogin() + " and password : " + user.getPassword());
        for (User uniqueUser : userList){
            System.out.println(uniqueUser);
        }
        return user;
    }
    private static User connectWithExistingUser(List<User> userList) {
        User userToGet = new User();
        String password = null;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Bonjour utilisateur veuillez choisir un login :");
        String reponse = scanner.nextLine();
        System.out.println("Nom choisi : " + reponse + ".Veuillez maintenant entrer votre mot de passe :");
        String reponsePass = scanner.nextLine();
        for (User user : userList) {
            if (user.getLogin().equals(reponse)) {
                 password = user.getPassword();
                 userToGet.setLogin(user.getLogin());
                 userToGet.setPassword(user.getPassword());
                 userToGet.setJwt_token(user.getJwt_token());
                 userToGet.setAgendas(user.getAgendas());
                 break;
            }
        }
        while (!password.equals(reponsePass)) {
            System.out.println("Mauvais mot de passe, réesayez..");
            System.out.println("Nom choisi : " + reponse + ".Veuillez maintenant entrer votre mot de passe :");
            String reponsePass2 = scanner.nextLine();
        }
            System.out.println("Mot de passe valide");
        System.out.println();
        return userToGet;
    }

}
