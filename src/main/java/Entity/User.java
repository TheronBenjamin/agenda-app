package Entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class User {

    public String login;
    private String password;
    private String jwt_token;
    private List<Agenda> agendas;

    @Override
    public String toString() {
        return "User{" +
                "login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", jwt_token='" + jwt_token + '\'' +
                ", agendas=" + agendas +
                '}';
    }
}
