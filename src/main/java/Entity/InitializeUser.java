package Entity;

import java.util.ArrayList;
import java.util.List;

public class InitializeUser {
    public InitializeUser() {
    }

    public static List<User>  Initialize() {
        User user1 = new User();
        user1.setLogin("root");
        user1.setPassword("root");
        user1.setJwt_token("ezdeeqeqzq");
        CreateAgendaList(user1);
        List<User> userList = new ArrayList<User>();
        userList.add(user1);
        return userList;
    }

    private static List<Agenda> CreateAgendaList(User user) {
        List<Contact> contactList = CreateContactList();
        Agenda agenda1 = new Agenda("agenda1", contactList);
        Agenda agenda2 = new Agenda( "agenda2", contactList);
        List<Agenda> agendaList = new ArrayList();
        agendaList.add(agenda1);
        agendaList.add(agenda2);
        user.setAgendas(agendaList);
        return agendaList;
    }

    private static List<Contact> CreateContactList() {
        List<AbstractContactDetail> contactDetails1 = CreateContactDetailList1();
        List<AbstractContactDetail> contactDetails2 = CreateContactDetailList2();
        Contact contact1 = new Contact("Contact1", contactDetails1);
        Contact contact2 = new Contact("Contact1", contactDetails2);
        List<Contact> contactList = new ArrayList();
        contactList.add(contact1);
        contactList.add(contact2);
        return contactList;
    }

    private static List<AbstractContactDetail> CreateContactDetailList1() {
        Phone phone = new Phone("022332332");
        Email email = new Email("root@gmail.com");
        List<AbstractContactDetail> contactDetails = new ArrayList();
        contactDetails.add(phone);
        contactDetails.add(email);
        return contactDetails;
    }

    private static List<AbstractContactDetail> CreateContactDetailList2() {
        Website website = new Website("http://site.com");
        Email email = new Email("site@gmail.com");
        List<AbstractContactDetail> contactDetails = new ArrayList();
        contactDetails.add(website);
        contactDetails.add(email);
        return contactDetails;
    }
}
