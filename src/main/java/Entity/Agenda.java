package Entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Agenda {

    public String name;
    public List<Contact> contacts;

    @Override
    public String toString() {
        return "Agenda{" +
                ", name=" + name +
                ", contacts=" + contacts +
                '}';
    }
}
