package Entity;

public class Adress extends AbstractContactDetail {

    public Adress(String value) {
        this.value = value;
    }

    @Override
    public void validate() {
        if (value.length() < 5) {
            throw new IllegalArgumentException("Adress must be at least 5 characters long");
        }
    }
}
