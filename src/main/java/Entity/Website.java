package Entity;

public class Website extends AbstractContactDetail {

    public Website(String value) {
        this.value = value;
    }

    @Override
    public void validate() {
        String websiteRegex = "^(https?:\\/\\/)?([\\da-z\\.-]+)\\.([a-z\\.]{2,6})([\\/\\w \\.-]*)*\\/?$";
        if (!value.matches(websiteRegex)) {
            System.out.println("Website must be in the format of " + websiteRegex);
        }
    }
}
