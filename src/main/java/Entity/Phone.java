package Entity;

public class Phone extends AbstractContactDetail {

    public Phone(String value) {
        this.value = value;
    }

    @Override
    public void validate() {
        String phoneRegex = "^(\\+\\d{1,3}[- ]?)?\\d{10}$";
        if (!value.matches(phoneRegex)) {
            System.out.println("Phone number must be 10 digits long");
        }
    }
}
