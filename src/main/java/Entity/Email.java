package Entity;

public class Email extends AbstractContactDetail {

    public Email(String value) {
        this.value = value;
    }

    @Override
    public void validate() {
        String mailRegex = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";
        if (!value.matches(mailRegex)) {
            System.out.println("Email must be in the format of " + mailRegex);
        }
    }
}
