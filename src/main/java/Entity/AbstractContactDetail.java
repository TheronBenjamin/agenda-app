package Entity;

public abstract class AbstractContactDetail {

    public String value;

    public abstract void validate();

    @Override
    public String toString() {
        return "AbstractContactDetail{" +
                "value='" + value + '\'' +
                '}';
    }
}
