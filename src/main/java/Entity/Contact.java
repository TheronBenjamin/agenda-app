package Entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class Contact {

    public String name;
    public List<AbstractContactDetail> contactDetails;

    @Override
    public String toString() {
        return "Contact{" +
                "name='" + name + '\'' +
                ", contactDetails=" + contactDetails +
                '}';
    }
}
